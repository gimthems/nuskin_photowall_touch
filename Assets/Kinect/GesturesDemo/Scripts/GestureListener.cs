using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
	// GUI Text to display the gesture messages.
	//public GUIText GestureInfo;
	
	//private bool swipeLeft;
	//private bool swipeRight;

	/*
	public bool IsSwipeLeft()
	{
		if(swipeLeft)
		{
			swipeLeft = false;
			return true;
		}
		
		return false;
	}
	
	public bool IsSwipeRight()
	{
		if(swipeRight)
		{
			swipeRight = false;
			return true;
		}
		
		return false;
	}*/

    private bool isWaveDetected;
    public bool IsWaveDetected()
    {
        if (isWaveDetected)
        {
            isWaveDetected = false;
            return true;
        }

        return false;
    }
    public Text txt_gesture;

    bool canDetect;
    public void SetCanDetect(bool _can)
    {
        canDetect = _can;
    }


    public void UserDetected(uint userId, int userIndex)
	{
        /*
        if (!canDetect)
        {
            Debug.Log("canDetect == false");
            return;
        }
        */
        // detect these user specific gestures
        KinectManager manager = KinectManager.Instance;
		
		//manager.DetectGesture(userId, KinectGestures.Gestures.SwipeLeft);
		//manager.DetectGesture(userId, KinectGestures.Gestures.SwipeRight);
        manager.DetectGesture(userId, KinectGestures.Gestures.Wave);
        /*
        if (GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = "Swipe left or right to change the slides.";
		}
        */
        if (txt_gesture)
        {
            txt_gesture.text = "Please Wave";
        }
	}
	
	public void UserLost(uint userId, int userIndex)
	{   
        /*
		if(GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = string.Empty;
		}*/
        if (txt_gesture)
        {
            txt_gesture.text = "User Lost";
        }
    }

	public void GestureInProgress(uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                              float progress, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
	{
		// don't do anything here
	}

	public bool GestureCompleted (uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
	{   /*
        if (!canDetect)
        {
            Debug.Log("canDetect == false");
            return false;
        }
        */
        /*
		string sGestureText = gesture + " Detected";       
		if(GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = sGestureText;
		}
        */
        if (txt_gesture)
        {
            txt_gesture.text = "Wave Detected!";
        }

        if (gesture == KinectGestures.Gestures.Wave)
        {
            isWaveDetected = true;
            Debug.Log("Gesture Listener: Wave Detected");
        }
        /*
        else if (gesture == KinectGestures.Gestures.SwipeLeft)
            swipeLeft = true;
        else if (gesture == KinectGestures.Gestures.SwipeRight)
            swipeRight = true;
        */
        return true;
	}

	public bool GestureCancelled (uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectWrapper.NuiSkeletonPositionIndex joint)
	{
		// don't do anything here, just reset the gesture state
		return true;
	}
	
}
