﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Threading;

public class PhotoTakingController : MonoBehaviour {

    // Cam to Take Photos
    public Camera cam_takePhotoToPrint;
	public RenderTexture rt_shot;
	public Material mat;
    public Camera cam_takePhotoToDisplay;

    // To show the taken photo
    public RawImage img_photoToDisplay;
    // To display what camera sees
    public RawImage img_camView;

    public int photoWidth = 860;
    public int photoHeight = 1040;
    public string savePath;
    string fullPath;

    [Header("Filters")]
    public RawImage img_filter_color;
    public RawImage img_filter_text;
    public RawImage img_filter_text_2;
    public RawImage img_filter_num_up;
    public RawImage img_filter_num_down;
    public bool hasFilterLoaded;

    // Global variables for texture2d
    Texture2D tex_shotToSave;
    RenderTexture tempRTtoSave;
    byte[] bytesForSave;

    Texture2D tempTexForFilter;
    byte[] bytesForFilter;


    /*
    private bool m_canShowTakenPhoto = false;   
    public bool CanShowTakenPhoto()
    {
        if (m_canShowTakenPhoto)
        {
            m_canShowTakenPhoto = false;
            return true;
        }
        return false;
    }
    */

    // photo is taken but not saved yet
    private bool m_isPhotoTaken = false;
    public bool IsPhotoTaken()
    {
        if (m_isPhotoTaken)
        {
            m_isPhotoTaken = false;
            return true;
        }
        return false;
    }

    private bool m_isPhotoSaved = false;
    public bool IsPhotoSaved()
    {
        if (m_isPhotoSaved)
        {
            m_isPhotoSaved = false;
            return true;
        }
        return false;
    }


    public GameLoop gameLoop;

    // Use this thread to encode png
    //Thread m_thread;

    // TEMP int for filter index
    //int dummyIndex = 5;

    void Start () {

        img_camView.texture = KinectManager.Instance.GetUsersClrTex();

    }
	
	void Update () {
		
	}

    public void TakePhoto()
    {
        
        StartCoroutine(E_TakePhoto());
    }

    IEnumerator E_TakePhoto()
    {
        yield return new WaitForEndOfFrame();

        #region Take_Photo_to_Display

        cam_takePhotoToDisplay.enabled = true;

        //RenderTexture tempRT_display = new RenderTexture(photoWidth, photoHeight, 24);
        //cam_takePhotoToDisplay.targetTexture = tempRT_display;
        cam_takePhotoToDisplay.Render();
        //RenderTexture.active = tempRT_display;
        //Texture2D tex_photoToDisplay = new Texture2D(photoWidth, photoHeight, TextureFormat.RGB24, false);
        //tex_photoToDisplay.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), 0, 0);
        //tex_photoToDisplay.Apply();

        cam_takePhotoToDisplay.enabled = false;

        // The photo is taken but not be saved yet
        // We show anim_shot, img_screenshot, and anim_printing first
        m_isPhotoTaken = true;
        yield return null;

        // Display the photo
        //img_photoToDisplay.texture = tex_photoToDisplay;
        //m_canShowTakenPhoto = true;
        //img_photoToDisplay.gameObject.SetActive(true);

        //cam_takePhotoToDisplay.targetTexture = null;
        //RenderTexture.active = null;
        //Destroy(tempRT_display);
        // TODO Destroy(tex_shotToSave);

        #endregion Take_Photo_to_Display

        #region Take_Photo_to_Print
		RenderTexture.active = rt_shot;
		tex_shotToSave = new Texture2D(1080, 1920, TextureFormat.RGB24, false);
		tex_shotToSave.ReadPixels(new Rect(0, 0, 1080, 1920), 0, 0);
		tex_shotToSave.Apply();

		yield return null;

		// Encode texture into PNG
		bytesForSave = tex_shotToSave.EncodeToPNG();

		// Save 
		string filename = GenerateFileName();
		fullPath = savePath + "\\" + filename;

		yield return null;

		try
		{
			System.IO.File.WriteAllBytes(fullPath, bytesForSave);
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			yield break;
		}

		// Check if the file exists
		if (System.IO.File.Exists(fullPath))
		{
			// The photo is saved and ready to be printed
			m_isPhotoSaved = true;

			cam_takePhotoToPrint.targetTexture = null;
			RenderTexture.active = null;
			Destroy(tempRTtoSave);
			Destroy(tex_shotToSave);
		}
		else
		{
			Debug.Log("File Not Exists. It may failed to save : " + fullPath);
		}
		/*

		/*
        tempRTtoSave = new RenderTexture(1080, 1920, 8);
        cam_takePhotoToPrint.targetTexture = tempRTtoSave;
        cam_takePhotoToPrint.Render();
        RenderTexture.active = tempRTtoSave;
        tex_shotToSave = new Texture2D(1080, 1920, TextureFormat.RGB24, false);
        tex_shotToSave.ReadPixels(new Rect(0, 0, 1080, 1920), 0, 0);
        tex_shotToSave.Apply();

        yield return null;

        // Encode texture into PNG
        bytesForSave = tex_shotToSave.EncodeToPNG();

        // Save 
        string filename = GenerateFileName();
        fullPath = savePath + "\\" + filename;

        yield return null;

        try
        {
            System.IO.File.WriteAllBytes(fullPath, bytesForSave);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            yield break;
        }
        
        // Check if the file exists
        if (System.IO.File.Exists(fullPath))
        {
            // The photo is saved and ready to be printed
            m_isPhotoSaved = true;

            cam_takePhotoToPrint.targetTexture = null;
            RenderTexture.active = null;
            Destroy(tempRTtoSave);
            Destroy(tex_shotToSave);
        }
        else
        {
            Debug.Log("File Not Exists. It may failed to save : " + fullPath);
        }
		*/
        #endregion Take_Photo_to_Print
        
    }

   
    // return the path of photo to be printed
    public string GetPhotoPath()
    {
        return fullPath;
    }

    string GenerateFileName()
    {
        //System.Globalization.CultureInfo.CurrentCulture.ClearCachedData();

        return string.Format("Photo_{0}_{1}_{2}.png",
                              gameLoop.playerCount.ToString("00000"),
                              DataLoader.numbers[gameLoop.playerCount],
                              System.DateTime.Now.ToString("MM-dd_HH-mm-ss"));
    }
    /*
    public void TEMP_LoadFiltersToTexture()
    {
        StartCoroutine(E_TEMP_LoadFiltersToTexture());
    }
    */
    public void LoadFiltersToTexture(int _index)
    {
        StartCoroutine(E_LoadFiltersToTexture(_index));
    }
    /*
    IEnumerator E_TEMP_LoadFiltersToTexture()
    {

        string fullFilterColorPath = DataLoader.filterColorPath + "\\C" + dummyIndex.ToString("00000") + ".png";
        img_filter_color.texture = LoadFilterFromFile(fullFilterColorPath);

        yield return null;

        string fullFilterTextPath = DataLoader.filterTextPath + "\\T" + dummyIndex.ToString("00000") + ".png";
        img_filter_text.texture = LoadFilterFromFile(fullFilterTextPath);
        img_filter_text_2.texture = img_filter_text.texture;

        yield return null;

        string fullFilterNumPath = DataLoader.filterNumPath + "\\N" + dummyIndex.ToString("00000") + ".png";
        img_filter_num_up.texture = LoadFilterFromFile(fullFilterNumPath);
        img_filter_num_down.texture = LoadFilterFromFile(fullFilterNumPath);

        dummyIndex++;
        if (dummyIndex > 10)
            dummyIndex = 2;

        hasFilterLoaded = true;
    }
    */
    IEnumerator E_LoadFiltersToTexture(int _index)
    {
        string fullFilterColorPath = DataLoader.filterColorPath + "\\C" + _index.ToString("00000") + ".png";
        img_filter_color.texture = LoadFilterFromFile(fullFilterColorPath);

        yield return null;

        string fullFilterTextPath = DataLoader.filterTextPath + "\\T" + _index.ToString("00000") + ".png";
        img_filter_text.texture = LoadFilterFromFile(fullFilterTextPath);
        img_filter_text_2.texture = img_filter_text.texture;

        yield return null;

        string fullFilterNumPath = DataLoader.filterNumPath + "\\N" + _index.ToString("00000") + ".png";
        img_filter_num_up.texture = LoadFilterFromFile(fullFilterNumPath);
        img_filter_num_down.texture = img_filter_num_up.texture;

        if(img_filter_color.texture && img_filter_text_2.texture && img_filter_num_up.texture)
        {
            hasFilterLoaded = true;
        }
        else
        {
            hasFilterLoaded = false;
        }
    }

    /*
    void LoadFilterFromFile(Texture2D _tex, string _filePath)
    {
        byte[] fileData;
        if (File.Exists(_filePath))
        {
            fileData = File.ReadAllBytes(_filePath);
            _tex = new Texture2D(2, 2);
            _tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            Debug.Log(_filePath);
        }
        else
        {
            Debug.LogError(_filePath + " Texture Path Not Found");
            _tex = null;
        }
    }
    */

    Texture2D LoadFilterFromFile(string _filePath)
    {
        //Texture2D tex = null;
        //byte[] fileData;

        if (File.Exists(_filePath))
        {
            //fileData = File.ReadAllBytes(_filePath);
            bytesForFilter = File.ReadAllBytes(_filePath);
            //tex = new Texture2D(2, 2);
            //tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            tempTexForFilter = new Texture2D(2, 2);
            //tempTexForFilter.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            tempTexForFilter.LoadImage(bytesForFilter); //..this will auto-resize the texture dimensions.
            Debug.Log(_filePath);
        }
        else
        {
            Debug.LogError(_filePath + " Texture Path Not Found");
        }
        return tempTexForFilter;
    }

    /*
    void Thread_EncodeToPng()
    {
        m_thread = new Thread(SaveToPng);
        m_thread.IsBackground = false;
        m_thread.Start();
    }

    void SaveToPng()
    {
        // Encode texture into PNG
        byte[] bytes = tex_shotToSave.EncodeToPNG();

        // Save 
        string filename = GenerateFileName();
        fullPath = savePath + "\\" + filename;

        try
        {
            System.IO.File.WriteAllBytes(fullPath, bytes);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
    
    void OnDisable()
    {
        if (m_thread != null)
            m_thread.Abort();
    }

    void OnDestroy()
    {
        if (m_thread != null)
            m_thread.Abort();
    }

    void OnApplicationQuit()
    {
        if (m_thread != null)
            m_thread.Abort();
    }
    */
}
