﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoop : MonoBehaviour {

    // Limitation: count to check paper and ink!
    //public int countToCheckPaperInk = 40;
    //private bool m_isPaperInkEnough;
    /*
    public bool IsPaperInkEnough()
    {
        if(playerCount > 0 && playerCount % countToCheckPaperInk == 0)
        {
            m_isPaperInkEnough = false;
            return false;
        }
        else
        {
            m_isPaperInkEnough = true;
            return true;
        }
    }
    */
    // Every time when we first load the game this variable should be true
    private bool m_isSaveJsonSuccess = true;
    public bool iSSaveJsonSuccess
    {
        get { return m_isSaveJsonSuccess; }
    }

    // Player Count
    [SerializeField]private int m_playerCount;
    public int playerCount { get { return m_playerCount; } }

    public DataLoader dataLoader;

    GameStateController m_gameStateController = new GameStateController();

    private bool canStart = false;

    void Awake()
    {
        GameObject.DontDestroyOnLoad(this.gameObject);

        dataLoader.LoadData();
        if (dataLoader.hasDataLoaded == false)
        {
            Debug.LogWarning("Something's wrong with DataLoader. Data Not Loaded.");
            // canStart remains false!
        }
        else
        {
            // Get data from dataLoader
            m_playerCount = DataLoader.playerCount;

            canStart = true;
        }

    }

    void Start ()
    {
        if (canStart)
        {
            // Set the beginning gameState
            m_gameStateController.SetState(new PrepareState(m_gameStateController));
        }
        else
        {
            Debug.Log("canStart is false!");
        } 
	}
	
	void Update () {

        if (canStart)
        {
            m_gameStateController.StateUpdate();
        }
    }

    public void AddPlayerCount()
    {
        m_isSaveJsonSuccess = dataLoader.SaveToJson(m_playerCount);
        m_playerCount += 1;
    }

}
