﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoTaker_backup: MonoBehaviour {

    // To display what camera sees
    public RawImage img_camView;
    //WebCamTexture webcamTexture;

    // The camera which sees only square and renders to the photo rig
    public Camera cam_renderToPhotoRig;

    // Take Photos
    public Camera cam_takePhoto;
    public int photoWidth = 1024;
    public int photoHeight = 1024;
    public string savePath;
    [Space]
    public RawImage img_filter_pattern;
    public string path_filter_pattern;
    public RawImage img_filter_text;
    public string path_filter_text;

    void Start () {

        /*
        webcamTexture = new WebCamTexture();
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
            Debug.Log(devices[i].name);

        img_camView.texture = webcamTexture; // Display what camera sees
        webcamTexture.Play();
        */

        img_camView.texture = KinectManager.Instance.GetUsersClrTex(); // Display what camera sees
        
    }
	
	void LateUpdate () {


	}

    public IEnumerator E_TakePhoto()
    {
        yield return new WaitForEndOfFrame();

        RenderTexture tempRT = new RenderTexture(860, 1040, 24);
        //RenderTexture currentRT = RenderTexture.active;
        //RenderTexture.active = cam_takePhoto.targetTexture;
        cam_takePhoto.targetTexture = tempRT;
        cam_takePhoto.Render();
        RenderTexture.active = tempRT;
        //cam_takePhoto.targetTexture = null;
        Texture2D tex_screenshot = new Texture2D(860, 1040, TextureFormat.RGB24, false);
        tex_screenshot.ReadPixels(new Rect(0, 0, 860, 1040), 0, 0);
        tex_screenshot.Apply();

        //RenderTexture.active = currentRT;

        // Encode texture into PNG
        byte[] bytes = tex_screenshot.EncodeToPNG();

        // Save 
        string filename = GenerateFileName();
        string path = savePath + filename;

        System.IO.File.WriteAllBytes(path, bytes);

        cam_takePhoto.targetTexture = null;
        RenderTexture.active = null;
        //Destroy(currentRT);
        Destroy(tempRT);
        Destroy(tex_screenshot);

        Debug.Log("Taken");
    }

    string GenerateFileName()
    {
        return string.Format("Photo_{0}.png",
                              GameManager.Instance.PlayerCount.ToString("00000"));
    }

    
}
