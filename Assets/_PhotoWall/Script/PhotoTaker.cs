﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PhotoTaker : MonoBehaviour {

    // To display what camera sees
    public RawImage img_camView;
    // To display screenshot
    public RawImage img_screenshotToDisplay;

    // Take Photos
    public Camera cam_takePhotoToPrint;
    public Camera cam_takePhotoToDisplay;
    Texture2D tex_photoToDisplay;
    //public Camera cam_takePhotoFilter;
    //public Camera cam_takePhotoToPrint;
    public int photoWidth = 860;
    public int photoHeight = 1040;
    public string savePath;
    bool isAlreadyTakingPhoto;
    [Space]
    public RawImage img_filter_color;
    //public string path_filter_color;
    public RawImage img_filter_text;
    //public string path_filter_text;
    public RawImage img_filter_num_up;
    public RawImage img_filter_num_down;
    //public string path_filter_num;
    public bool hasFilterLoaded;

    // Count Down
    [Header("Count Down")]
    public float countDownSeconds = 5f;
    float countDownTimer;
    public Text txt_countdown;
    bool isAlreadyCounting;
    bool isInPose;

    public CmdControl cmdControl;

    //public Animator shotAnim;

    bool isCoRunning_E_TakePhotoToSaveAndPrint;
    bool isCoRunning_E_TakePhotoToDisplay;

    GameManager gameManager;

    void Start () {

        gameManager = FindObjectOfType<GameManager>();

        img_camView.texture = KinectManager.Instance.GetUsersClrTex(); // Display what camera sees

        // Count Down
        countDownTimer = countDownSeconds;

        hasFilterLoaded = false;
    }
	
	void LateUpdate () {


	}

    public void Init()
    {
        savePath = DataLoader.photoPath;
    }

    public IEnumerator E_TakePhoto()
    {
        if (isAlreadyTakingPhoto)
        {
            Debug.Log("PhotoTaker: E_TakePhoto() Coroutine is Still Running!");
            yield break;
        }
        isAlreadyTakingPhoto = true;

        // Play the shot anim
        //shotAnim.gameObject.SetActive(true);
        //shotAnim.Play("UI_shot");

        yield return new WaitForEndOfFrame();

        StartCoroutine(E_TakePhotoToDisplay());
        yield return StartCoroutine(E_TakePhotoToPrint());

        isAlreadyTakingPhoto = false;
        gameManager.AddPlayerCount(); // TEMP Add Player Count
        GameManager.gameState = GameManager.GameState.PrintingState;
        
    }

    // Take a photo, encode it to png and print it
    IEnumerator E_TakePhotoToPrint()
    {
        if (isCoRunning_E_TakePhotoToSaveAndPrint)
        {
            Debug.Log("E_TakePhotoToPrint is Already Running!");
            yield break;
        }
        isCoRunning_E_TakePhotoToSaveAndPrint = true;

        RenderTexture tempRTtoSave = new RenderTexture(1080, 1920, 24);
        cam_takePhotoToPrint.targetTexture = tempRTtoSave;
        cam_takePhotoToPrint.Render();
        RenderTexture.active = tempRTtoSave;
        Texture2D tex_shotToSave = new Texture2D(1080, 1920, TextureFormat.RGB24, false);
        tex_shotToSave.ReadPixels(new Rect(0, 0, 1080, 1920), 0, 0);
        tex_shotToSave.Apply();

        // Encode texture into PNG
        byte[] bytes = tex_shotToSave.EncodeToPNG();

        // Save 
        string filename = GenerateFileName();
        string path = savePath + "\\" + filename;

        System.IO.File.WriteAllBytes(path, bytes);

        // Print!
        if (System.IO.File.Exists(path))
        {
            FileInfo fileInfo = new FileInfo(path);
            if (IsFileLocked(fileInfo) == false)
            {
                //string command = string.Format(@"rundll32 C:\WINDOWS\system32\shimgvw.dll,ImageView_PrintTo ""{0}"" ""Canon TS8100 series""", path);
                string command = "rundll32 C:\\WINDOWS\\system32\\shimgvw.dll,ImageView_PrintTo " + "\"" + path + "\"" + " " + "\"" + "Canon TS8100 series" + "\"";
                Debug.Log(command);
                cmdControl.Thread_Command_Print(command);
            }
            else
            {
                Debug.LogError(path + " File Locked! Can Not Print.");
            }
        }
        else
        {
            Debug.LogError(path + " File Not Exists! Can Not Print.");
        }

        cam_takePhotoToPrint.targetTexture = null;
        RenderTexture.active = null;
        Destroy(tempRTtoSave);
        Destroy(tex_shotToSave);

        isCoRunning_E_TakePhotoToSaveAndPrint = false;
    }

    // Take photo to display
    IEnumerator E_TakePhotoToDisplay()
    {
        if (isCoRunning_E_TakePhotoToDisplay)
        {
            Debug.Log("E_TakePhotoToDisplay is Already Running!");
            yield break;
        }
        isCoRunning_E_TakePhotoToDisplay = true;

        RenderTexture tempRT_display = new RenderTexture(photoWidth, photoHeight, 24);
        cam_takePhotoToDisplay.targetTexture = tempRT_display;
        cam_takePhotoToDisplay.Render();
        RenderTexture.active = tempRT_display;
        //Texture2D tex_photoToDisplay = new Texture2D(photoWidth, photoHeight, TextureFormat.RGB24, false);
        tex_photoToDisplay = new Texture2D(photoWidth, photoHeight, TextureFormat.RGB24, false);
        tex_photoToDisplay.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), 0, 0);
        tex_photoToDisplay.Apply();
        
        // Display
        img_screenshotToDisplay.texture = tex_photoToDisplay;
        DisplayScreenshot(true);

        cam_takePhotoToDisplay.targetTexture = null;
        RenderTexture.active = null;
        Destroy(tempRT_display);
        // Destroy(tex_shotToSave);

        isCoRunning_E_TakePhotoToDisplay = false;
    }

    //IEnumerator 

    /*
    public IEnumerator E_TakePhoto()
    {
        
        //yield return E_CountDown();
        

        if (isAlreadyTakingPhoto)
        {
            Debug.Log("PhotoTaker: E_TakePhoto() Coroutine is Still Running!");
            yield break;
        }
        isAlreadyTakingPhoto = true;

        yield return new WaitForEndOfFrame();

        RenderTexture tempRT_Filter = new RenderTexture(photoWidth, photoHeight, 24);
        //RenderTexture currentRT = RenderTexture.active;
        //RenderTexture.active = cam_takePhotoToPrint.targetTexture;
        cam_takePhotoFilter.targetTexture = tempRT_Filter;
        cam_takePhotoFilter.Render();
        RenderTexture.active = tempRT_Filter;
        //cam_takePhotoToPrint.targetTexture = null;
        Texture2D tex_shotToSave = new Texture2D(photoWidth * 2, photoHeight, TextureFormat.RGB24, false);
        tex_shotToSave.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), 0, 0);
        tex_shotToSave.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), photoWidth, 0);
        tex_shotToSave.Apply();

        RenderTexture tempRT = new RenderTexture(photoWidth, photoHeight, 24);
        cam_takePhotoToPrint.targetTexture = tempRT;
        cam_takePhotoToPrint.Render();
        RenderTexture.active = tempRT;
        Texture2D tex_photoToDisplay = new Texture2D(photoWidth, photoHeight, TextureFormat.RGB24, false);
        tex_photoToDisplay.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), 0, 0);
        tex_photoToDisplay.Apply();
        img_screenshotToDisplay.texture = tex_photoToDisplay;
        DisplayScreenshot(true);

        // Encode texture into PNG
        byte[] bytes = tex_shotToSave.EncodeToPNG();

        // Save 
        string filename = GenerateFileName();
        string path = savePath + "\\" + filename;

        System.IO.File.WriteAllBytes(path, bytes);

        // Print!
        if (System.IO.File.Exists(path))
        {
            Debug.Log("Exists");
            FileInfo fileInfo = new FileInfo(path);
            if(IsFileLocked(fileInfo) == false)
            {
                //string command = string.Format(@"rundll32 C:\WINDOWS\system32\shimgvw.dll,ImageView_PrintTo ""{0}"" ""Canon TS8100 series""", path);
                string command = "rundll32 C:\\WINDOWS\\system32\\shimgvw.dll,ImageView_PrintTo " + "\"" + path + "\"" + " " + "\"" + "Canon TS8100 series" + "\"";
                Debug.Log(command);
                CmdControl.Command_Print(command);
                Debug.Log("Not Locked");
            }
        }

        cam_takePhotoFilter.targetTexture = null;
		cam_takePhotoToPrint.targetTexture = null;
        RenderTexture.active = null;
        
        Destroy(tempRT_Filter);
        Destroy(tempRT);
        Destroy(tex_shotToSave);
        //Destroy(tex_photoToDisplay);
        // TODO Destroy the textures
        isAlreadyTakingPhoto = false;
        GameManager.gameState = GameManager.GameState.PrintingState;

        Debug.Log("Taken: " + path);
        //GameManager.gameState = GameManager.GameState.PrintingState;
    }
    */
    string GenerateFileName()
    {
        return string.Format("Photo_{0}_{1}.png",
                              GameManager.Instance.PlayerCount.ToString("00000"),
                              System.DateTime.Now.ToString("MM-dd_HH-mm-ss"));
    }

    bool IsFileLocked(FileInfo _file)
    {
        FileStream stream = null;

        try
        {
            stream = _file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException e)
        {
            //the file is unavailable because it is:
            //still being written to
            //or being processed by another thread
            //or does not exist (has already been processed)
            Debug.Log(e.Message);
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        //file is not locked
        return false;
    }
    

    public void DisplayScreenshot(bool _active)
    {
        img_screenshotToDisplay.gameObject.SetActive(_active);
    }

    public IEnumerator E_CountDown()
    {
        countDownTimer = countDownSeconds;

        // To check if this coroutine is already running
        if (isAlreadyCounting)
        {
            Debug.Log("Is Already Counting Down!");
            yield break;
        }
        isAlreadyCounting = true;

        while (countDownTimer >= 0f)
        {
            countDownTimer -= Time.deltaTime;
            txt_countdown.text = countDownTimer.ToString("F0");
            yield return null;
        }
        // Reset countDownTimer

        countDownTimer = countDownSeconds;
        isAlreadyCounting = false;
    }

    Texture2D LoadFilterFromFile(string _filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(_filePath))
        {
            fileData = File.ReadAllBytes(_filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            Debug.Log(_filePath);
        }
        else
        {
            Debug.LogError(_filePath + " Texture Path Not Found");
        }
        return tex;
    }

    public void LoadFiltersToTexture(int _index)
    {
        string fullFilterColorPath = DataLoader.filterColorPath + "\\C" + _index.ToString("00000") + ".png";
        img_filter_color.texture = LoadFilterFromFile(fullFilterColorPath);

        string fullFilterTextPath = DataLoader.filterTextPath + "\\T" + _index.ToString("00000") + ".png";
        img_filter_text.texture = LoadFilterFromFile(fullFilterTextPath);

        string fullFilterNumPath = DataLoader.filterNumPath + "\\N" + _index.ToString("00000") + ".png";
        img_filter_num_up.texture = LoadFilterFromFile(fullFilterNumPath);
        img_filter_num_down.texture = img_filter_num_up.texture;

        hasFilterLoaded = true;
    }
}
