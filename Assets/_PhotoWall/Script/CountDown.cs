﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    public float countDownSeconds = 5f;
    float countDownTimer;
    public Text txt_countdown;
    bool isAlreadyCounting;
    bool isInPose;

    void Start () {

        countDownTimer = countDownSeconds;
       
	}
	
	void Update () {

        
    }

    public IEnumerator E_CountDown()
    {
        
        // To check if this coroutine is already running
        if (isAlreadyCounting)
        {
            Debug.Log("Is Already Counting Down!");
            yield break;
        }
        isAlreadyCounting = true;
                
        while (countDownTimer >= 0f)
        {
            countDownTimer -= Time.deltaTime;
            txt_countdown.text = countDownTimer.ToString("F0");
            yield return null;
        }
        // Reset countDownTimer

        countDownTimer = countDownSeconds;
        isAlreadyCounting = false;
    }
}
