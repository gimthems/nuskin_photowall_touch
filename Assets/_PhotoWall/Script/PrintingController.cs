﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintingController : MonoBehaviour {

    public CmdControl cmdControl;

	public Text countDownNumText;
	float countDown;
	public void ResetCountDown(){
		countDown = waitTime;
	}

    public float waitTime = 50f;
    private bool m_canStartCounting = false;
    public bool canStartCounting
    {
        get { return m_canStartCounting; }
        set { m_canStartCounting = value; }
    }
    private float timer = 0f;

    private bool m_isWaitFinished = false;
    public bool IsWaitFinished()
    {
        if (m_isWaitFinished)
        {
            m_isWaitFinished = false;
            return true;
        }
        return false;
    }

    public void Print(string _path)
    {
        string command = "rundll32 C:\\WINDOWS\\system32\\shimgvw.dll,ImageView_PrintTo " + "\"" + _path + "\"" + " " + "\"" + "Canon TS8100 series" + "\"";
        Debug.Log("Command: " + command);
        cmdControl.Thread_Command_Print(command);
    }

	void Start () {
		countDown = waitTime;
	}
	
	void Update () {

        if (canStartCounting)
        {
			
            timer += Time.deltaTime;
			countDown -= Time.deltaTime;


            if (timer >= waitTime)
            {
                timer = 0f;

                m_canStartCounting = false;

                m_isWaitFinished = true;
            }
        }
		countDownNumText.text = countDown.ToString("00");

	}
}
