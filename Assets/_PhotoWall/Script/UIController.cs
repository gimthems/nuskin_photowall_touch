﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public Animator anim_countDown;

    private bool m_countDownFinished = false;
    public bool CountDownFinished()
    {
        if (m_countDownFinished)
        {
            m_countDownFinished = false;
            return true;
        }
        return false;
    }

    public Animator anim_shot;
    public GameObject img_mainMenu;
    public GameObject img_screenShot;
    public GameObject animObj_takingPhoto;
    public GameObject animObj_printing;
    public GameObject animObj_done;
    public GameObject pausePanel;
    public GameObject endPanel;
    public GameObject errorPanel;

    void Start () {
		
	}
	
	void Update () {
		
	}

    public void CountDown()
    {
        StartCoroutine(E_CountDown());
    }

    IEnumerator E_CountDown()
    {
        anim_countDown.gameObject.SetActive(true);
        anim_countDown.Play("UI_CountDown");

        while(anim_countDown.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            // Wait for count down finishing
            yield return null;
        }

        m_countDownFinished = true;
        anim_countDown.gameObject.SetActive(false);
    }

    public void OnStateBegin(Extension.GameState _state)
    {
        switch (_state)
        {
            // Note that we show mainMenu nad hide screenshot in Prepare state
            case Extension.GameState.Prepare:
                img_mainMenu.SetActive(true);
                img_screenShot.SetActive(false);
                break;
            case Extension.GameState.Detection:
                img_mainMenu.SetActive(true);
                img_screenShot.SetActive(false);
                break;
            case Extension.GameState.TakePhoto:
                animObj_takingPhoto.SetActive(true);
                break;
            case Extension.GameState.Print:
                animObj_printing.SetActive(true);
                break;
            case Extension.GameState.Done:
                // Note that we deactivate anim_shot here
                anim_shot.gameObject.SetActive(false);
                animObj_done.SetActive(true);
                break;
        }
    }

    public void OnStateEnd(Extension.GameState _state)
    {
        switch (_state)
        {
            case Extension.GameState.Prepare:
                pausePanel.SetActive(false);
                break;
            case Extension.GameState.Detection:
                img_mainMenu.SetActive(false);
                break;
            case Extension.GameState.TakePhoto:
                //animObj_takingPhoto.SetActive(false);
                //anim_shot.gameObject.SetActive(true);
                //anim_shot.Play("UI_shot");
                //img_screenShot.SetActive(true);
                break;
            case Extension.GameState.Print:
                animObj_printing.SetActive(false);
                break;
            case Extension.GameState.Done:
                animObj_done.SetActive(false);
                break;
        }
    }
    
    public void ShowShotAnimAndPhoto()
    {
        anim_shot.gameObject.SetActive(true);
        anim_shot.Play("UI_shot");
        img_screenShot.SetActive(true);
        animObj_takingPhoto.SetActive(false);
        animObj_printing.SetActive(true);
    }
    
	/*
    public void ShowAnimPrintingPhoto()
    {
        animObj_takingPhoto.SetActive(false);
        animObj_printing.SetActive(true);
    }

    public void ShowPhoto()
    {
        img_screenShot.SetActive(true);
    }
	*/
    public void SetPausePanel(bool _active)
    {
        pausePanel.SetActive(_active);
    }

    public void ShowEndPanel()
    {
        endPanel.SetActive(true);
    }

    public void ShowErrorPanel()
    {
        errorPanel.SetActive(true);
    }
}
