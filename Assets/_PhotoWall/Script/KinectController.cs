﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KinectController : MonoBehaviour {

	public bool useTouch;

    // Settings
    public float posXThreshold = 0.3f;
    public float posZThreshold = 2f;
    public float orientationThreshold = 0.2f;
    public float standTimeThreshold = 1f;
    
	// UI to show wave status
	//public GameObject icon;

    //public GestureListener gestureListener;

    private bool m_isWaveDetected;
    //public bool isWaveDetected { get { return m_isWaveDetected; } }
    public bool isWaveDetected()
    {
        if(m_isWaveDetected)
        {
            m_isWaveDetected = false; // Reset m_isWaveDetected!
            return true;
        }
        return false;
    }

    private bool canDetect;
    public void CanKinectDetect(bool _can)
    {
        canDetect = _can;
    }

    private KinectManager kinectManager;
    private uint userID;

    public float handUpTimeThrs = 2f;
    private float handUpTimer;
    
    void Start () {

        //icon.SetActive(false);

        if (useTouch)
        {
            Debug.Log("TOUCH MODE");
            return;
        }

        if (kinectManager == null)
            kinectManager = KinectManager.Instance;

        if (kinectManager.IsInitialized() == false)
        {
            // Show Please check Kinect!
            Debug.LogError("Something's Wrong with Kinect! Game Cannot Continue!");
        }

    }
	
	void Update () {
    
        // Touch Mode
        if (useTouch)
        {
            return;
        }
        
        
        if (canDetect)
        {
            if (kinectManager == null)
                kinectManager = KinectManager.Instance;

            if (kinectManager.IsInitialized() == false)
            {
                // Show Please check Kinect!
                Debug.LogError("Something's Wrong with Kinect! Game Cannot Continue!");
                return;
            }

            if (kinectManager.IsUserDetected())
            {
                userID = kinectManager.GetPlayer1ID();
                // Check if user's position is valid
                if (Mathf.Abs(kinectManager.GetUserPosition(userID).x) <= posXThreshold &&
                kinectManager.GetUserPosition(userID).z <= posZThreshold)
                {
                    // Check if user is facing enough to cam
                    if (kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft) &&
                        kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight) &&
                        kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.Head))
                    {
                        if (Mathf.Abs(kinectManager.GetUserOrientation(userID, false).y) < orientationThreshold)
                        {                 

                            if ((kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft) &&
                                kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight) &&
                                kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.Head)) ||
                                (kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight) &&
                                kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft) &&
                                kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.Head)))
                            {
								//icon.SetActive(true);

                                Vector3 posHandRight = kinectManager.GetJointPosition(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight);
                                Vector3 posHandLeft = kinectManager.GetJointPosition(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft);
                                Vector3 posShoulderRight = kinectManager.GetJointPosition(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight);
                                Vector3 posShoulderLeft = kinectManager.GetJointPosition(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft);

                                if((posHandRight.y >= posShoulderLeft.y) || (posHandLeft.y >= posShoulderRight.y))
                                {
                                    Debug.Log("Hand Up");
                                    handUpTimer += Time.deltaTime;
                                    if(handUpTimer >= handUpTimeThrs)
                                    {
                                        handUpTimer = 0f;
                                        m_isWaveDetected = true;
										//icon.SetActive(false);
                                        kinectManager.ClearKinectUsers();
                                    }
                                }
                                else
                                {
                                    handUpTimer = 0f;
                                }
                            }
                            else
                            {
                                handUpTimer = 0f;
                            }

                        }
                        else
                        {
							//icon.SetActive(false);
                            handUpTimer = 0f;
                        }
                    }
                    else
                    {
						//icon.SetActive(false);
                        handUpTimer = 0f;
                    }
                }
                else
                {
					//icon.SetActive(false);
                    kinectManager.ClearKinectUsers();
                }
            }
            else
            {
				//icon.SetActive(false);
                kinectManager.ClearKinectUsers();
            }
        }
        
    }
}
