﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TestSceenshot : MonoBehaviour {

	public RawImage img_camView;
	public string path = "C:\\Users\\User\\Desktop\\TempPhoto";

	public int photoWidth = 1024;
	public int photoHeight = 768;

	void Start(){
		img_camView.texture = KinectManager.Instance.GetUsersClrTex();

	}

	// Use this for initialization
	IEnumerator Take () 
	{

		yield return new WaitForEndOfFrame ();

		Texture2D tex_shotToSave = new Texture2D(photoWidth, photoHeight, TextureFormat.RGB24, false);
		tex_shotToSave.ReadPixels(new Rect(0, 0, photoWidth, photoHeight), 0, 0);
		tex_shotToSave.Apply();

		yield return null;

		// Encode texture into PNG
		byte[] bytesForSave = tex_shotToSave.EncodeToPNG();

		// Save 
		string filename = GenerateFileName();
		string fullPath = path + "\\" + filename;

		try
		{
			System.IO.File.WriteAllBytes(fullPath, bytesForSave);
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			yield break;
		}
	}
	
	string GenerateFileName()
	{
		//System.Globalization.CultureInfo.CurrentCulture.ClearCachedData();

		return string.Format("Photo_{0}.png",
			
			System.DateTime.Now.ToString("MM-dd_HH-mm-ss"));
	}

	void LateUpdate()
	{

		if (Input.GetKeyDown (KeyCode.A)) {
			StartCoroutine(Take());
		}
	}
}
