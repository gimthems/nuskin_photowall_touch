﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class DataLoader : MonoBehaviour {

    // To store the data from json and get accessed by other classes
    public static int playerCount;
    public static string photoPath;
    public static string filterColorPath;
    public static string filterTextPath;
    public static string filterNumPath;

    string jsonPath;
    ConfigData configData;

    private bool m_hasDataLoaded = false;
    public bool hasDataLoaded { get { return m_hasDataLoaded; } }

    #region CSV
    public static int[] numbers;

    public string csvFileName = "Test-NewLine.csv";
    #endregion CSV

    public void LoadData()
    {
        try
        {
            jsonPath = Application.streamingAssetsPath + "\\ConfigData.json";
            string jsonString = File.ReadAllText(jsonPath);
            configData = JsonUtility.FromJson<ConfigData>(jsonString);

            // Store the data from json and get accessed by other classes
            playerCount = configData.PlayerCount;
            photoPath = configData.PhotoPath;
            filterColorPath = configData.FilterColorPath;
            filterTextPath = configData.FilterTextPath;
            filterNumPath = configData.FilterNumPath;
        }
        catch(Exception e)
        {
            Debug.LogError(e.Message);
            return;
        }

        #region CSV
        string[] fileTexts;

        try
        {
            string csvPath = Application.streamingAssetsPath + "/" + csvFileName;
            fileTexts = File.ReadAllLines(csvPath);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            return;
        }

        numbers = new int[fileTexts.Length];
        for (int i = 0; i < fileTexts.Length; i++)
        {
            int temp = 0;
            bool convertSuccess = Int32.TryParse(fileTexts[i], out temp);
            if (convertSuccess)
            {
                numbers[i] = temp;
            }
            else
            {
                Debug.LogError("CSV Convert " + fileTexts[i] + " Failed");
                return;
            }
        }
        #endregion CSV

        Debug.Log("Data Loaded.");
        m_hasDataLoaded = true;
    }


	void LoadNumbersFromCSV()
    {
        string csvPath = Application.streamingAssetsPath + "/" + csvFileName;
        string[] fileTexts = File.ReadAllLines(csvPath);
        numbers = new int[fileTexts.Length];
        for (int i = 0; i < fileTexts.Length; i++)
        {
            int temp = 0;
            bool convertSuccess = Int32.TryParse(fileTexts[i], out temp);
            if (convertSuccess)
            {
                numbers[i] = temp;
            }
            else
            {
                Debug.LogError("CSV Convert " + fileTexts[i] + " Failed");
                return;
            }
        }
    }
    /*
    public void SaveToJsonV(int _playerCount)
    {
        playerCount = _playerCount;
        string newConfigData = JsonUtility.ToJson(configData);
        Debug.LogWarning("SAVESAVESAVE");
    }
    */
    public bool SaveToJson(int _playerCount)
    {
        try
        {
            configData.PlayerCount = _playerCount;
            string newConfigData = JsonUtility.ToJson(configData);
            File.WriteAllText(jsonPath, newConfigData);
            Debug.Log("PlayerCount Saved: " + _playerCount);
            return true;
        }
        catch(Exception e)
        {
            Debug.LogError("Json Save Failed: " + e.Message);
            return false;
        }
    }
}

[System.Serializable]
public class ConfigData
{
    public int PlayerCount;
    public string PhotoPath;
    public string FilterColorPath;
    public string FilterTextPath;
    public string FilterNumPath;

}
