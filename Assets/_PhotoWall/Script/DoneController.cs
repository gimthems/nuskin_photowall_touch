﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoneController : MonoBehaviour {

    public float waitTime = 5f;
    private bool m_canStartCounting = false;
    public bool canStartCounting
    {
        get { return m_canStartCounting; }
        set { m_canStartCounting = value; }
    }
    private float timer = 0f;

    private bool m_isWaitFinished = false;
    public bool IsWaitFinished()
    {
        if (m_isWaitFinished)
        {
            m_isWaitFinished = false;
            return true;
        }
        return false;
    }

    private bool m_isForceLeave;
    public bool isForceLeave
    {
        get { return m_isForceLeave; }
        set { m_isForceLeave = value; }
    }

    void Start () {
		
	}
	
	void Update () {

        if (canStartCounting)
        {
            if (m_isForceLeave)
            {
                m_isForceLeave = false;
                m_canStartCounting = false;
                m_isWaitFinished = true;
            }
            else
            {
                timer += Time.deltaTime;
                if (timer >= waitTime)
                {
                    timer = 0f;
                    m_canStartCounting = false;
                    m_isWaitFinished = true;
                }
            }
           
        }

    }

}
