﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Extension {

	public enum GameState { Prepare, Detection, TakePhoto, Print, Done}

    public static bool IsFileLocked(string _filePath)
    {
        FileInfo fileInfo = new FileInfo(_filePath);

        FileStream stream = null;

        try
        {
            stream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException e)
        {
            //the file is unavailable because it is:
            //still being written to
            //or being processed by another thread
            //or does not exist (has already been processed)
            Debug.Log(e.Message);
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        //file is not locked
        return false;
    }

    /*
    public static bool IsFileLocked(string _filePath)
    {
        if (System.IO.File.Exists(_filePath))
        {
            FileInfo fileInfo = new FileInfo(_filePath);

            FileStream stream = null;

            try
            {
                stream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException e)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                Debug.Log(e.Message);
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        else
        {
            Debug.LogError("File Not Exist: " + _filePath);
            return false;
        }    
    }
    */
}
