﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePhotoState : IGameState {

    // Constructor
    public TakePhotoState(GameStateController _controller):base(_controller)
    {
        this.gameState = Extension.GameState.TakePhoto;
    }

    ControllerHolder holder;
    UIController uiController;
    PhotoTakingController photoTakingController;
    GameLoop gameLoop;

    public override void StateBegin()
    {
        // Get reference
        holder = GameObject.FindWithTag("Holder").GetComponent<ControllerHolder>();
        uiController = holder.uiController;
        photoTakingController = holder.photoTakingController;
        gameLoop = holder.gameLoop;
        if (holder && uiController && photoTakingController && gameLoop)
            isReferenceReady = true;
        else
            Debug.Log(this.ToString() + " Reference May Not Be Found!");

        // Show Anim Taking Photo
        uiController.OnStateBegin(Extension.GameState.TakePhoto);

        // Show Anim Count Down and wait for the anim finish
        uiController.CountDown();
    
    }

    public override void StateEnd()
    {
        // Show the Shot Anim and the Taken Photo
        // Hide Anim Taking Photo
        uiController.OnStateEnd(Extension.GameState.TakePhoto);

        // Increase playerCount
        gameLoop.AddPlayerCount();
        
    }

    public override void StateUpdate()
    {
        if (isReferenceReady == false)
        {
            Debug.Log(this.ToString() + " Reference Not Ready Yet.");
            return;
        }

        // if the count down anim finish, we can take photo
        if (uiController.CountDownFinished())
        {
            //uiController.ShowAnimPrintingPhoto();
            photoTakingController.TakePhoto();
        }

        // If the photo is taken (but may not be saved yet)
        if (photoTakingController.IsPhotoTaken())
        {
            uiController.ShowShotAnimAndPhoto();
            //uiController.ShowPhoto();
            Debug.Log("Taken Photo is Shown. Saving...");
        }

        // If the photo is saved and ready to be printed, set gameState to PrintState
        if (photoTakingController.IsPhotoSaved())
        {
            // To store the path and pass it to the PrintingController in the next state
            holder.photoPath = photoTakingController.GetPhotoPath();
            Debug.Log("Taken Photo is Saved.");
            m_controller.SetState(new PrintState(m_controller));
        }
    }
}
