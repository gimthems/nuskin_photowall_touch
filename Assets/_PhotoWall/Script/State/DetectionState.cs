﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionState : IGameState {

    // Constructor
    public DetectionState(GameStateController _controller):base(_controller)
    {
        this.gameState = Extension.GameState.Detection;
    }

    ControllerHolder holder;
    UIController uiController;
    KinectController kinectController;

	//public bool useTouch;

    public override void StateBegin()
    {
        // Get reference
        holder = GameObject.FindWithTag("Holder").GetComponent<ControllerHolder>();
        kinectController = holder.kinectController;
        uiController = holder.uiController;

        if(holder && kinectController && uiController)
            isReferenceReady = true;
        else
            Debug.Log(this.ToString() + " Reference May Not Be Found!");


        // UI Main Menu may has been shown in Prepare State
        uiController.OnStateBegin(Extension.GameState.Detection);

        if (kinectController.useTouch) return;
        // Ask kinectController to start detecting
        kinectController.CanKinectDetect(true);
       
    }

    public override void StateEnd()
    {
        // Hide UI Main Menu 
        uiController.OnStateEnd(Extension.GameState.Detection);

        if (kinectController.useTouch) return;
        // Ask kinectController to stop detecting
        kinectController.CanKinectDetect(false);
    }

    public override void StateUpdate()
    {
        if (isReferenceReady == false)
        {
            Debug.Log(this.ToString() + " Reference Not Ready Yet.");
            return;
        }

        if (holder.isRepair)
        {
            uiController.SetPausePanel(true);
            return;
        }
        else
        {
            uiController.SetPausePanel(false);
        }

        if (kinectController.useTouch)
        {
			if (Input.GetMouseButtonDown (0))
            {
				m_controller.SetState (new TakePhotoState (m_controller));
			}
		}


        /*
		else 
		{
			// if "wave" is detected then m_controller.SetState(new TakePhotoState(m_controller))
			if (kinectController.isWaveDetected())
			{
				m_controller.SetState(new TakePhotoState(m_controller));
			}
		}
        */
    }
}
