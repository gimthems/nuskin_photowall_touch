﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController {

    // "Context" in Design Pattern - State

    private IGameState m_State;
    private bool runBegin = false;

    // Constructor
    public GameStateController()
    {

    }

    public void SetState(IGameState _newState)
    {
        Debug.Log("SetState: " + _newState.ToString());
        runBegin = false;

        // Load Scene??
        

        // Ask the last gameState to end
        if(m_State != null)
        {
            m_State.StateEnd();
        }

        m_State = _newState;
    }

    public void StateUpdate()
    {
        // if still loading or something
        // return

        // Ask the new gameState to begin
        if(m_State != null && runBegin == false)
        {
            m_State.StateBegin();
            runBegin = true;
        }

        if(m_State != null)
        {
            m_State.StateUpdate();
        }
    }

}
