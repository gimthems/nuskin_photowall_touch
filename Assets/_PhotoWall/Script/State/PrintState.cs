﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PrintState : IGameState {

    // Constructor
    public PrintState(GameStateController _controller):base(_controller)
    {
        this.gameState = Extension.GameState.Print;
    }

    ControllerHolder holder;
    UIController uiController;
    PrintingController printingController;
    DoneController doneController;

    public override void StateBegin()
    {
        // Get reference
        holder = GameObject.FindWithTag("Holder").GetComponent<ControllerHolder>();
        uiController = holder.uiController;
        printingController = holder.printingController;
        doneController = holder.doneController;
        if (holder && uiController && printingController && doneController)
            isReferenceReady = true;
        else
            Debug.Log(this.ToString() + " Reference May Not Be Found!");

        // Show Anim Printing
        uiController.OnStateBegin(Extension.GameState.Print);

        // Print!!
        if (Extension.IsFileLocked(holder.photoPath) == false)
        {
            printingController.Print(holder.photoPath);
        }
        else
        {
            Debug.LogError("File Seems To Be Locked! " + holder.photoPath);
            return;
        }

        // Ask printingController to start counting down
		printingController.ResetCountDown();
        printingController.canStartCounting = true;
    }

    public override void StateEnd()
    {
        // Hide Anim Printing
        uiController.OnStateEnd(Extension.GameState.Print);
    }

    public override void StateUpdate()
    {
        if (isReferenceReady == false)
        {
            Debug.Log(this.ToString() + " Reference Not Ready Yet.");
            return;
        }

        if (holder.isRepair)
        {
            // Show Pause Panel
            uiController.SetPausePanel(true);
            return;
        }
        else
        {
            uiController.SetPausePanel(false);
            // TODO when repair done we should go to mainmenu
            // Note we cannot write this logic here
        }

        // User can touch screen to force to main menu
        if (Input.GetMouseButtonDown(0))
        {
            doneController.isForceLeave = true;
            m_controller.SetState(new DoneState(m_controller));
            return;
        }

        // wait for printing done and then set to GameState.Done
        if (printingController.IsWaitFinished())
        {
            m_controller.SetState(new DoneState(m_controller));
        }
    }

    
}
