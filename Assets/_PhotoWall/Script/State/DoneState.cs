﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoneState : IGameState {

    // Constructor
    public DoneState(GameStateController _controller):base(_controller)
    {
        this.gameState = Extension.GameState.Done;
    }

    ControllerHolder holder;
    UIController uiController;
    DoneController doneController;

    public override void StateBegin()
    {
        // Get reference
        holder = GameObject.FindWithTag("Holder").GetComponent<ControllerHolder>();
        uiController = holder.uiController;
        doneController = holder.doneController;
        if (holder && uiController && doneController)
            isReferenceReady = true;
        else
            Debug.Log(this.ToString() + " Reference May Not Be Found!");

        // Show UI Printing Done
        uiController.OnStateBegin(Extension.GameState.Done);

        // Ask doneController to start count
        doneController.canStartCounting = true;
    }

    public override void StateEnd()
    {
        Resources.UnloadUnusedAssets();

        // Hide UI Printing Done
        uiController.OnStateEnd(Extension.GameState.Done);
    }

    public override void StateUpdate()
    {
        if (isReferenceReady == false)
        {
            Debug.Log(this.ToString() + " Reference Not Ready Yet.");
            return;
        }

        if (holder.isRepair)
        {
            // Show Pause Panel
            uiController.SetPausePanel(true);
            return;
        }
        else
        {
            uiController.SetPausePanel(false);
        }

        // If we wait enough 
        if (doneController.IsWaitFinished())
        {
            m_controller.SetState(new PrepareState(m_controller));
        }
    }
}
