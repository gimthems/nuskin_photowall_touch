﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IGameState {

    private Extension.GameState m_gameState;
    public Extension.GameState gameState
    {
        get { return m_gameState; }
        set { m_gameState = value; }
    }

    protected GameStateController m_controller = null;

    // Constructor
    public IGameState(GameStateController _controller)
    {
        m_controller = _controller;
    }

    protected bool isReferenceReady = false;

    public virtual void StateBegin()
    {

    }

    public virtual void StateEnd()
    {

    }

    public virtual void StateUpdate()
    {

    }
}
