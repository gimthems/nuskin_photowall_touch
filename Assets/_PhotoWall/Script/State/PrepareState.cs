﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareState : IGameState {

    // Constructor
    public PrepareState(GameStateController _controller):base(_controller)
    {
        this.gameState = Extension.GameState.Prepare;
    }

    ControllerHolder holder;
    UIController uiController;
    GameLoop gameLoop;
    PhotoTakingController photoTakingController;

    private bool isSaveJsonSuccess;

    //private bool isPaperInkEnough;
    // TEMP To Test this program when there's no people in front of the pc
    //public int dummyTime = 1;
    //float dummyTimer;

    private bool isGameEnd;

    public override void StateBegin()
    {
        // Get reference
        holder = GameObject.FindWithTag("Holder").GetComponent<ControllerHolder>();
        uiController = holder.uiController;
        gameLoop = holder.gameLoop;
        photoTakingController = holder.photoTakingController;

        if (holder && uiController && gameLoop && photoTakingController)
            isReferenceReady = true;
        else
            Debug.Log(this.ToString() + " Reference May Not Be Found!");

        // Show MainMenu and hide img_screenShot
        uiController.OnStateBegin(Extension.GameState.Prepare);

        // Check if save playerCount to json successful
        isSaveJsonSuccess = gameLoop.iSSaveJsonSuccess;
        if(isSaveJsonSuccess == false)
        {
            Debug.LogError("Saving to Json Failed when playerCount is " + gameLoop.playerCount);
            uiController.ShowErrorPanel();
            return;
        }

        // Check if our numbers is enough
        if (gameLoop.playerCount > DataLoader.numbers.Length - 1)
        {
            Debug.Log("No Numbers Left! Player Count: " + gameLoop.playerCount + ". Game Ends.");
            isGameEnd = true;
            uiController.ShowEndPanel();
            return;
        }
        // Load Filter
        photoTakingController.LoadFiltersToTexture(DataLoader.numbers[gameLoop.playerCount]);
        /*
        // Check if paper and ink is enough
        if (gameLoop.IsPaperInkEnough() == false)
        {
            holder.isRepair = true;
            // Show Pause Panel
            uiController.SetPausePanel(true);
            // isPaperInkEnough remains false!
        }
        else // is enough
        {
            isPaperInkEnough = true;
        }
        */
    }

    public override void StateEnd()
    {
        uiController.OnStateEnd(Extension.GameState.Prepare);
    }

    public override void StateUpdate()
    {
        if (isSaveJsonSuccess == false)
        {
            Debug.LogError("Saving to Json Failed when playerCount is " + gameLoop.playerCount);
            return; // If json saving fail, game freeze here!!
        }

        if (isGameEnd)
        {
            Debug.LogWarning("Game Ends!");
            return;
        }

        m_controller.SetState(new DetectionState(m_controller));

        /*
        if (isPaperInkEnough)
        {
            m_controller.SetState(new DetectionState(m_controller));
        }
        else // paper and ink not enough
        {
            holder.isRepair = true;
            
            if (Input.GetKeyDown(KeyCode.Y))
            {
                isPaperInkEnough = true; // this will set gameState to DetectionState
            }
        }

        if (holder.isRepair)
        {
            // Show Pause Panel
            uiController.SetPausePanel(true);
            return;
        }
        else
        {
            uiController.SetPausePanel(false);
        }

        */
    }
}
