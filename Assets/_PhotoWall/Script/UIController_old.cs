﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController_old : MonoBehaviour {

    //public GameObject img_photoFrameMask;
    public GameObject img_camView;
    //public GameObject img_screenshot;
    //public GameObject icon_TakingPhoto;
    //public GameObject icon_txtPrinting;
    //public GameObject icon_txtDone;

    public Animator countDownAnim;
    public Animator shotAnim;

    public UItool uitool_mainMenu;
    public UItool uitool_screenshot;
    public UItool uitool_icon_TakingPhoto;
    public UItool uitool_icon_txtPrinting;
    public UItool uitool_icon_txtDone;

    

    public bool hasMainMenuIn;
    bool setUI_MainMenu_coRunning;

    public bool hasCountDownFinish;
    bool setUI_TakingPhoto_coRunning;

    // TEMP
    public PhotoTaker script_photoTaker;

    

    void Start () {
       
       // Init();
    }
	
	void Update () {
		
        if(GameManager.gameState == GameManager.GameState.DetectStandState)
        {
            uitool_mainMenu.gameObject.SetActive(true);
            uitool_screenshot.gameObject.SetActive(false);
            uitool_screenshot.gameObject.SetActive(false);
            uitool_icon_txtPrinting.gameObject.SetActive(false);
            uitool_icon_txtDone.gameObject.SetActive(false);
        }
        else if(GameManager.gameState == GameManager.GameState.DetectWaveState)
        {
            uitool_mainMenu.gameObject.SetActive(true);
        }
        else if(GameManager.gameState == GameManager.GameState.CountDownState)
        {
            SetUI_TakingPhoto();
        }
        else if(GameManager.gameState == GameManager.GameState.PrintingState)
        {
            // Play the shot anim
            //shotAnim.gameObject.SetActive(true);
            //shotAnim.Play("UI_shot");

            uitool_screenshot.gameObject.SetActive(true);
            img_camView.SetActive(false);
            uitool_icon_TakingPhoto.gameObject.SetActive(false);
            uitool_icon_txtPrinting.gameObject.SetActive(true);
        }
        else if (GameManager.gameState == GameManager.GameState.DoneState)
        {
            shotAnim.gameObject.SetActive(false);
            StartCoroutine(E_SetUI_DoneState());
        }
    }

    bool isSetUI_DoneState_coRunning;
    IEnumerator E_SetUI_DoneState()
    {
        if (isSetUI_DoneState_coRunning)
        {
            Debug.Log("isSetUI_DoneState_coRunning is Running");
            yield break;
        }
        isSetUI_DoneState_coRunning = true;

        uitool_icon_txtPrinting.gameObject.SetActive(false);
        uitool_icon_txtDone.gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        GameManager.gameState = GameManager.GameState.DetectStandState;

        isSetUI_DoneState_coRunning = false;
    }

    public void Init()
    {

        //SetUI_MainMenu();

        /*
        // Hide all
        img_photoFrameMask.SetActive(false);
        img_camView.SetActive(false);
        img_screenshot.SetActive(false);
        icon_TakingPhoto.SetActive(false);
        icon_txtPrinting.SetActive(false);
        icon_txtDone.SetActive(false);

        GameManager.OnEnterDetectStandState += SetUI_MainMenu;
        GameManager.OnEnterCountDownState += SetUI_TakingPhoto;
        GameManager.OnEnterPrintingState += SetUI_Printing;
        */
    }
    /*
    public void SetUI_MainMenu()
    {
        StartCoroutine(E_SetUI_MainMenu());
    }
    */
    /*
    IEnumerator E_SetUI_MainMenu()
    {
        if (setUI_MainMenu_coRunning)
        {
            Debug.Log("setUI_MainMenu_coRunning is Running!");
            yield break;
        }
        setUI_MainMenu_coRunning = true;

        //img_photoFrameMask.SetActive(false);
        //icon_txtDone.SetActive(false);

        // Only Show MainMenu
        yield return StartCoroutine(uitool_mainMenu.E_Fade());

        img_camView.SetActive(false);
        countDownAnim.gameObject.SetActive(false);
        uitool_screenshot.SetAlpha(0f);
        uitool_icon_TakingPhoto.SetAlpha(0f);
        uitool_icon_txtPrinting.SetAlpha(0f);
        uitool_icon_txtDone.SetAlpha(0f);

        hasMainMenuIn = true;
        setUI_MainMenu_coRunning = false;

    }
    */
    public void SetUI_TakingPhoto()
    {
        hasMainMenuIn = false;
        StartCoroutine(E_SetUI_TakingPhoto());
    }

    public bool HasCountDownFinish()
    {
        return hasCountDownFinish;
    }

    IEnumerator E_SetUI_TakingPhoto()
    {
        //img_photoFrameMask.SetActive(true);
        //img_camView.SetActive(true);
        //icon_TakingPhoto.SetActive(true);

        if (setUI_TakingPhoto_coRunning)
        {
            //Debug.Log("E_SetUI_TakingPhoto is Running!");
            yield break;
        }
        setUI_TakingPhoto_coRunning = true;

        img_camView.SetActive(true);
        // icon_TakingPhoto Fade In
        //uitool_icon_TakingPhoto.E_Fade();
        uitool_icon_TakingPhoto.gameObject.SetActive(true);
        // MainMenu Fade Out
        //yield return StartCoroutine(uitool_mainMenu.E_Fade());
        uitool_mainMenu.gameObject.SetActive(false);
        // Count Down Anim
        countDownAnim.gameObject.SetActive(true);
        countDownAnim.Play("UI_CountDown");
        while (countDownAnim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        {
            // Wait for count down finishing
            yield return null;
        }

        Debug.LogWarning("ANIM COUNT DOWN DONE!");

        hasCountDownFinish = true;
        // Play the shot anim
        shotAnim.gameObject.SetActive(true);
        shotAnim.Play("UI_shot");

        yield return StartCoroutine(script_photoTaker.E_TakePhoto());
        countDownAnim.gameObject.SetActive(false);
        setUI_TakingPhoto_coRunning = false;
               
    }

    public void SetUI_Printing()
    {
        /*
        img_screenshot.SetActive(true);
        img_camView.SetActive(false);

        icon_TakingPhoto.SetActive(false);
        icon_txtPrinting.SetActive(true);
        */
        //StartCoroutine(E_SetUI_Printing());
    //}

    //IEnumerator E_SetUI_Printing()
    //{
        // icon_TakingPhoto Fade Out
        //StartCoroutine(uitool_icon_TakingPhoto.E_Fade());
        // icon_txtPrinting Fade In
        //StartCoroutine(uitool_icon_txtPrinting.E_Fade());
        // uitool_screenshot In!!!
       // uitool_screenshot.SetAlpha(1);

       // yield return null;
    }
    
    public void SetUI_Done()
    {
        /*
        icon_txtPrinting.SetActive(false);
        icon_txtDone.SetActive(true);
        */
    }
    
}
