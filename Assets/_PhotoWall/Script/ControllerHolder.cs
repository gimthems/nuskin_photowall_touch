﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHolder : MonoBehaviour {

    public KinectController kinectController;
    public PhotoTakingController photoTakingController;
    // Temp string to store the path of photo to be printed
    [HideInInspector]public string photoPath;
    public UIController uiController;
    public PrintingController printingController;
    public DoneController doneController;
    public GameLoop gameLoop;
    public DataLoader dataLoader;

    public bool isRepair;

    private void Update()
    {
        if (isRepair == false)
        {
            if (Input.GetKeyDown(KeyCode.R)){
                isRepair = true;
            }
        }
        else{
            if (Input.GetKeyDown(KeyCode.Y)){
                isRepair = false;
            }
        }
        
    }
}
