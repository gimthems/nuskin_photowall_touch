﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UItool : MonoBehaviour {

    public float duration = 1f;
    //public AnimationCurve fadeCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public CanvasGroup canvasGroup;
    bool isFading;

    void Awake()
    {
        //canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start () {
		
	}
	
	void Update () {
		
	}
    
    public void SetAlpha(float _a)
    {
        canvasGroup.alpha = _a;
    }

    public IEnumerator E_Fade(float _targetAlpha)
    {
        if (isFading)
        {
            Debug.Log(gameObject.name + " is Already Fading!");
            yield break;
        }
        isFading = true;

        float targetAlpha = _targetAlpha;
        // if (canvasGroup.alpha > 0.5f) then canvasGroup.alpha is 1 so its targetAlpha is 0

        float timer = 0f;
        while (timer < 1f)
        {
            timer += Time.deltaTime * 1f / duration;
            //float val = fadeCurve.Evaluate(timer) ;
            float val = Mathf.Lerp(canvasGroup.alpha, targetAlpha, timer);
            canvasGroup.alpha = val;

            if (Mathf.Abs(targetAlpha - canvasGroup.alpha) < 0.05f)
                canvasGroup.alpha = targetAlpha;

            yield return null;
        }

        isFading = false;
    }

    /*
    public IEnumerator E_Fade()
    {
        if (isFading)
        {
            Debug.Log(gameObject.name + " is Already Fading!");
            yield break;
        }
        isFading = true;

        float targetAlpha = (canvasGroup.alpha > 0.5f) ? 0 : 1;
        // if (canvasGroup.alpha > 0.5f) then canvasGroup.alpha is 1 so its targetAlpha is 0

        float timer = 0f;
        while (timer < 1f)
        {
            timer += Time.deltaTime * 1f / duration;
            //float val = fadeCurve.Evaluate(timer) ;
            float val = Mathf.Lerp(canvasGroup.alpha, targetAlpha, timer);
            canvasGroup.alpha = val;

            if (Mathf.Abs(targetAlpha - canvasGroup.alpha) < 0.05f)
                canvasGroup.alpha = targetAlpha;
            
            yield return null;
        }

        isFading = false;
    }
    */
}
