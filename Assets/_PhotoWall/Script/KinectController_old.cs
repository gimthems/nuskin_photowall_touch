﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KinectController_old: MonoBehaviour
{
    
    public float posXThreshold = 0.3f;
    public float posZThreshold = 2f;
    public float orientationThreshold = 0.2f;
    KinectManager kinectManager;
    uint userID;

    public float standTimeThreshold = 1f;
    float standTimer;

    public GestureListener gestureListener;
    bool canDetectGesture;

    void Start () {

        // Count Down
        //countDownTimer = countDownSeconds;

    }
	
	void Update () {

    }

    public void Detect()
    {
        if (kinectManager == null)
            kinectManager = KinectManager.Instance;

        if (kinectManager.IsInitialized() == false)
        {
            // Show Please check Kinect!
            Debug.LogError("Something's Wrong with Kinect! Game Cannot Continue!");
            return;
        }

        if (kinectManager.IsUserDetected())
        {
            userID = kinectManager.GetPlayer1ID();
            // Check if user's position is valid
            if (Mathf.Abs(kinectManager.GetUserPosition(userID).x) <= posXThreshold &&
            kinectManager.GetUserPosition(userID).z <= posZThreshold)
            {
                // Check if user is facing enough to cam
                if (kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft) &&
                    kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight) &&
                    kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.Head))
                {
                    if (Mathf.Abs(kinectManager.GetUserOrientation(userID, false).y) < orientationThreshold)
                    {
                        standTimer += Time.deltaTime;
                        while (standTimer < standTimeThreshold)
                        {
                            //Debug.Log("Checking Stand");
                            return;
                        }
                        standTimer = 0f;
                        GameManager.gameState = GameManager.GameState.DetectWaveState;
                    }
                    else
                    {
                        standTimer = 0f;
                    }
                }
                else
                {
                    standTimer = 0f;
                }
            }
            else
            {
                standTimer = 0f;
                kinectManager.ClearKinectUsers();
            }
        }
        else
        {
            standTimer = 0f;
            kinectManager.ClearKinectUsers();
        }
    }

    public void DetectStand()
    {
        if (kinectManager == null)
            kinectManager = KinectManager.Instance;

        if (kinectManager.IsInitialized() == false)
        {
            // Show Please check Kinect!
            Debug.LogError("Something's Wrong with Kinect! Game Cannot Continue!");
            return;
        }

        if (kinectManager.IsUserDetected())
        {
            userID = kinectManager.GetPlayer1ID();
            // Check if user's position is valid
            if (Mathf.Abs(kinectManager.GetUserPosition(userID).x) <= posXThreshold &&
            kinectManager.GetUserPosition(userID).z <= posZThreshold)
            {
                // Check if user is facing enough to cam
                if (kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft) &&
                    kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight) &&
                    kinectManager.IsJointTracked(userID, (int)KinectWrapper.NuiSkeletonPositionIndex.Head))
                {
                    if (Mathf.Abs(kinectManager.GetUserOrientation(userID, false).y) < orientationThreshold)
                    {
                        standTimer += Time.deltaTime;
                        while (standTimer < standTimeThreshold)
                        {
                            //Debug.Log("Checking Stand");
                            return;
                        }
                        standTimer = 0f;
                        GameManager.gameState = GameManager.GameState.DetectWaveState;
                    }
                    else
                    {
                        standTimer = 0f;
                    }
                }
                else
                {
                    standTimer = 0f;
                }
            }
            else
            {
                standTimer = 0f;
                kinectManager.ClearKinectUsers();
            }
        }
        else
        {
            standTimer = 0f;
            kinectManager.ClearKinectUsers();
        }
    }

    public void DetectWave()
    {
        if (kinectManager.IsUserDetected())
        {
            userID = kinectManager.GetPlayer1ID();
            // Check if user's position is valid
            if (Mathf.Abs(kinectManager.GetUserPosition(userID).x) <= posXThreshold &&
            kinectManager.GetUserPosition(userID).z <= posZThreshold)
            {
                canDetectGesture = true;
                //kinectManager.DetectGesture(userID, KinectGestures.Gestures.Wave);
                //gestureListener.SetCanDetect(true);

                if (canDetectGesture)
                {
                    if (gestureListener.IsWaveDetected())
                    {
                        Debug.LogWarning("DETECTED");
                        canDetectGesture = false;
                        GameManager.gameState = GameManager.GameState.CountDownState;
                    }
                }                
            }
            else
            {
                //gestureListener.SetCanDetect(false);
                canDetectGesture = false;
                GameManager.gameState = GameManager.GameState.DetectStandState;
            }
        }
        else
        {
            //gestureListener.SetCanDetect(false);
            canDetectGesture = false;
            GameManager.gameState = GameManager.GameState.DetectStandState;
        }
    }
    
}
