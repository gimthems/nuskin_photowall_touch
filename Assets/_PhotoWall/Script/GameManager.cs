﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    // Singleton
    public static GameManager Instance { get; private set; }

    // Player Count
    public int PlayerCount { get { return playerCount; }}
    [SerializeField]int playerCount;

    public PhotoTaker script_photoTaker;
    bool canTakePhoto;

    // Kinect
    [Header("Kinect")]
    public KinectController_old kinectController;
    KinectManager kinectManager;
    
    // Limitation
    [Header("Limitation")]
    public int maxPaperCount = 50;
    //public int maxInkCount = 110;
    //bool canPlay; // if we have no paper or ink we can not play the game
    bool confirm;
    //bool isPaperEnough;
    //bool isInkEnough;

    public enum GameState { DetectStandState, DetectWaveState, CountDownState, PrintingState, DoneState}
    public static GameState gameState;

    // Print
    [Header("Print")]
    public float waitForPrinting = 50f;
    bool isAlreadyWaitForPrinting = false;

    // UI
    [Header("Game UI")]
    public UIController_old script_UIConntroller;

    // Events 
    public delegate void EnterDetectStandState();
    public static event EnterDetectStandState OnEnterDetectStandState;
    public delegate void EnterDetectWaveState();
    public static event EnterDetectWaveState OnEnterDetectWaveState;
    public delegate void EnterCountDownState();
    public static event EnterCountDownState OnEnterCountDownState;
    public delegate void EnterPrintingState();
    public static event EnterPrintingState OnEnterPrintingState;

    public DataLoader dataLoader;

    // Pause
    public GameObject pannel_pause;

    // Debug UI
    [Header("Debug UI")]
    public Canvas canvas_debug;
    public Text txt_calibration;
    public Text txt_gameState;

    void Awake()
    {

        // Singleton
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        

        //canPlay = true;
        //isPaperEnough = true;
        //isInkEnough = true;

        // Count Down
        //countDownTimer = countDownSeconds;

        dataLoader.LoadData();

        playerCount = DataLoader.playerCount;
    }

    void Start () {

        script_photoTaker.Init();
    }

    void Update()
    {
        if(kinectManager == null)
            kinectManager = KinectManager.Instance;

        if (kinectManager.IsInitialized() == false)
        {
            // Show Please check Kinect!
            Debug.LogError("Something's Wrong with Kinect! Game Cannot Continue!");
            return;
        }

        

        /* TODO After adding ink or paper contitue the game! */

        switch (gameState)
        {
            case GameState.DetectStandState:

                if(confirm == false)
                {
                    // Check are paper and ink enough?
                    if (playerCount % maxPaperCount == 0)
                    {
                        // Activate the pause panel 
                        pannel_pause.SetActive(true);

                        if (Input.GetKeyDown(KeyCode.Y))
                        {
                            pannel_pause.SetActive(false);
                            confirm = true;
                            return;
                        }
                    }
                }
                               
                //script_UIConntroller.SetUI_MainMenu();
                script_photoTaker.DisplayScreenshot(false);
                if (script_photoTaker.hasFilterLoaded == false)
                    script_photoTaker.LoadFiltersToTexture(PlayerCount);
                kinectController.DetectStand();
                //if (script_UIConntroller.hasMainMenuIn)
                //{
                //    kinectController.DetectStand();
                //}
                break;

            case GameState.DetectWaveState:
                kinectController.DetectWave();
                break;

            case GameState.CountDownState:

                confirm = false; // TEMPPPPPPPPPPPPPPPPPPPPPPP!!!!!!!!!!!!!!!

                break;

            case GameState.PrintingState:
                //script_UIConntroller.SetUI_Printing();
                StartCoroutine(E_WaitForPrintingDone());
                script_photoTaker.hasFilterLoaded = false;
                break;
            case GameState.DoneState:

                break;

        }

        // Update Debug UI
        if (canvas_debug.gameObject.activeInHierarchy)
        {
            txt_gameState.text = gameState.ToString();
        }
    }

    void LateUpdate () {
        /*
        if (canTakePhoto)
        {
            canTakePhoto = false;
            gameState = GameState.PrintingState;
            playerCount++;
            StartCoroutine(script_photoTaker.E_TakePhoto());
        }
        */
    }

    IEnumerator E_WaitForPrintingDone()
    {
        if (isAlreadyWaitForPrinting)
        {
            Debug.Log("Is Already Waiting for Printing!");
            yield break;
        }
        
        yield return new WaitForSeconds(waitForPrinting);
        //script_UIConntroller.SetUI_Done();
        //yield return new WaitForSeconds(5f);
        //gameState = GameState.DetectStandState;
        gameState = GameState.DoneState;

        isAlreadyWaitForPrinting = false;
    }

    /*
    IEnumerator E_CountDownAndTakePhoto()
    {
        //yield return StartCoroutine(script_countDown.E_CountDown());
        //yield return StartCoroutine(script_photoTaker.E_TakePhoto());
        canTakePhoto = true;
        gameState = GameState.PrintingState;
        playerCount++;
        yield return null;
    }
    */

    // Called by KinectManager
    public void SetCalibrationText(string _text)
    {
        if (canvas_debug.gameObject.activeInHierarchy)
        {
            txt_calibration.text = _text;
        }
    }

    // TEMP called by PhotoTaker
    public void AddPlayerCount()
    {
        playerCount += 1;
    }
}
