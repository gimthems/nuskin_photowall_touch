﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

public class CmdControl : MonoBehaviour {

    Thread m_thread;

    void Start () {

        //string command = string.Format(@"rundll32 C:\WINDOWS\system32\shimgvw.dll,ImageView_PrintTo ""{0}"" ""Canon TS8100 series""", @"C: \Users\Gim\Documents\Tripper_Work\2018_NuSkin\PhotoWall\Photo_Taken\Photo_00009.png");
        //UnityEngine.Debug.Log(command);

        //Command_Print(@"rundll32 C:\WINDOWS\system32\shimgvw.dll,ImageView_PrintTo ""C:\Users\Gim\Documents\Tripper_Work\2018_NuSkin\PhotoWall\Photo_Taken\Photo_00009.png"" ""Canon TS8100 series""");
    }
	
	void Update () {
        
	}

    public void Thread_Command_Print(string _command)
    {
        m_thread = new Thread(delegate () { Command_Print(_command); });
        m_thread.IsBackground = false;
        m_thread.Start();
    }

    void Command_Print(string _command)
    {
        try
        {
            Process myProcess = new Process();
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.FileName = "cmd.exe";
            myProcess.StartInfo.Arguments = "/c " + _command;
            myProcess.EnableRaisingEvents = true;
            myProcess.Start();
            myProcess.WaitForExit();
            int ExitCode = myProcess.ExitCode;
            print(ExitCode);
            
        }
        catch (Exception e)
        {
            print(e);
        }
    }


    public static void Command(string input)
    {
        var processInfo = new ProcessStartInfo("cmd.exe", @"cd " + input);
        //var processInfo = new ProcessStartInfo("cmd.exe", @"ffmpeg -i " + input + @" -acodec libvorbis -vcodec libtheora -f ogg " + input.Split('.')[0] + @".ogg");
        //processInfo.CreateNoWindow = true;
        processInfo.UseShellExecute = false;

        //processInfo.WindowStyle = ProcessWindowStyle.Hidden;

        var process = Process.Start(processInfo);

        process.WaitForExit();
        process.Close();
    }

    void OnDisable()
    {
        if(m_thread != null)
            m_thread.Abort();
    }

    void OnDestroy()
    {
        if (m_thread != null)
            m_thread.Abort();
    }

    void OnApplicationQuit()
    {
        if (m_thread != null)
            m_thread.Abort();
    }

    void GetCommandLineArgs()
    {
        UnityEngine.Debug.Log(string.Format("CommandLine: {0}", System.Environment.CommandLine));

        String[] arguments = System.Environment.GetCommandLineArgs();
        UnityEngine.Debug.Log(string.Format("GetCommandLineArgs: {0}", String.Join(", ", arguments)));

        ;
        //return arguments;
    }
}
